import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';


@Component({
  selector: 'app-to-euro-convert-component',
  templateUrl: './to-euro-convert-component.component.html',
  styleUrls: ['./to-euro-convert-component.component.css']
})
export class ToEuroConvertComponentComponent implements OnInit {
@Input() montantConvert: number;
 @Output() valeurEuro = new EventEmitter();
  // tslint:disable-next-line:typedef
  getConvert(){
    return this.montantConvert * 3;
  }

 constructor() {
  }


  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  sendEvent(){
    this.valeurEuro.emit(
      this.montantConvert * 3
    );

  }
}
