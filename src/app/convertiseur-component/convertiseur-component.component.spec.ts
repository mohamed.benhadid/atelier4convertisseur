import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvertiseurComponentComponent } from './convertiseur-component.component';

describe('ConvertiseurComponentComponent', () => {
  let component: ConvertiseurComponentComponent;
  let fixture: ComponentFixture<ConvertiseurComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConvertiseurComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertiseurComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
